'use strict';

//get access to the mongoose library
const mongoose = require('mongoose');

//set the mongoose schema property to a variable
const Schema = mongoose.Schema;

//create a schema constructor (class) and set it to a variable, this schema will provide properties for our model
const UserSchema = new Schema({
  name: String
});

//set the entire mongoose model we created to a variable. this takes 2 parameters, a name ('users' in this case), and the schema constructor.
const User = mongoose.model('user', UserSchema);

//export the user variable exposing the entire mongoose model constructor (class) to any application we need it in through the require method.
module.exports = User;